#!/bin/bash
# This scripts install some basic stuff and prepares a VM to be used.
# It doesn't do fancy magic, just some basic stuff in which I lose time
# every time I boot up a new vagrant machine.

# This is intended to be launched from ansible

### CHECKS THE PACKAGE MANAGER ###

if [ -f /etc/os-release ]; then
	echo "checking the os-release..."
	if [[ ! -z `grep -nr debian /etc/os-release` ]]; then
		PKG_MGR='apt'
	elif [[ ! -z `grep -nri redhat /etc/os-release` ]]; then
		PKG_MGR='yum'
	fi
fi

echo "SETUP: INFO: package manager in use is $PKG_MGR"


### INSTALL BASIC PROGRAMS ###
basic_programs(){
	sudo $PKG_MGR update
	sudo $PKG_MGR upgrade -y
	sudo $PKG_MGR autoremove -y
	sudo $PKG_MGR install vim tmux htop tree git curl wget python3 virtualenv python3-pip ansible gcc g++ make zsh -y
	echo "setting up tmux..."
	cd
	git clone https://github.com/gpakosz/.tmux.git
	ln -s -f .tmux/.tmux.conf
	cp .tmux/.tmux.conf.local .

}

### SETUP VIM ###
vim_setup(){
	cp files/vimrc ~/.vimrc
	vim +PluginInstall +qall
}

### SETUP BASHRC ###
bash_setup(){
	echo "set -o vi" >> ~/.bashrc
	echo "alias ll='ls -l'" >> ~/.bashrc
	echo "alias ia='ip -c a'" >> ~/.bashrc
	echo "alias vi='vim'" >> ~/.bashrc
}

### SETUP ZSH ###
zsh_setup(){
	sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
	echo "set -o vi" >> ~/.zshrc
	echo "alias ll='ls -l'" >> ~/.zshrc
	echo "alias ia='ip -c a'" >> ~/.zshrc
	echo "alias vi='vim'" >> ~/.zshrc
}

### DOCKER INSTALLATION ###
docker_install(){
	# DEBIAN 9
	if [ "apt" == $PKG_MGR ]; then
		sudo apt-get remove docker docker-engine docker.io
		sudo apt-get update
		sudo apt-get install \
     			apt-transport-https \
     			ca-certificates \
     			curl \
     			gnupg2 \
     			software-properties-common
		curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
		sudo add-apt-repository \
			"deb [arch=amd64] https://download.docker.com/linux/debian \
   			$(lsb_release -cs) \
   			stable"
		sudo apt-get update
		sudo apt-get install docker-ce
	fi
}
### MAIN ###
basic_programs 
docker_install
vim_setup
bash_setup
zsh_setup

